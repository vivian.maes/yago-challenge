import 'package:flutter/material.dart';

import '../shared/app_colors.dart';
import '../shared/ui_helpers.dart';

ThemeData purpleTheme = ThemeData(
  useMaterial3: true,
  colorSchemeSeed: const Color(AppColors.primary),
  fontFamily: UiHelpers.fontFamily,
);
