import 'package:flutter/material.dart';

import '../app_colors.dart';
import '../styles.dart';

class SuiText extends StatelessWidget {
  const SuiText.tinyStyle(this.text,
      {Key? key, TextAlign align = TextAlign.start})
      : style = Styles.tinyStyle,
        alignment = align,
        super(key: key);

  const SuiText.smallStyle(this.text,
      {Key? key, TextAlign align = TextAlign.start})
      : style = Styles.smallStyle,
        alignment = align,
        super(key: key);

  const SuiText.regularStyle(this.text,
      {Key? key, TextAlign align = TextAlign.start})
      : style = Styles.regularStyle,
        alignment = align,
        super(key: key);

  SuiText.body(this.text,
      {Key? key,
      Color color = const Color(AppColors.neutral),
      TextAlign align = TextAlign.start})
      : style = Styles.bodyStyle.copyWith(color: color),
        alignment = align,
        super(key: key);

  const SuiText.mediumStyle(this.text,
      {Key? key, TextAlign align = TextAlign.start})
      : style = Styles.mediumStyle,
        alignment = align,
        super(key: key);

  const SuiText.largeStyle(this.text,
      {Key? key, TextAlign align = TextAlign.start})
      : style = Styles.largeStyle,
        alignment = align,
        super(key: key);

  final String text;
  final TextStyle style;
  final TextAlign alignment;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: style,
      textAlign: alignment,
    );
  }
}
