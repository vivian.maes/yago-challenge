import 'package:flutter/material.dart';

import 'sui_text.dart';

class SuiErrorMessage extends StatelessWidget {
  const SuiErrorMessage({required this.message, this.details, Key? key})
      : super(key: key);

  final String message;
  final List<String>? details;

  makeListDetails() {
    return Column(
        children: details!
            .map(
              (str) => Row(children: [SuiText.regularStyle(str)]),
            )
            .toList());
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: const Color.fromARGB(255, 255, 228, 216),
        border: Border.all(
          width: 2,
          color: const Color.fromARGB(255, 250, 174, 141),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Row(children: [
              SuiText.mediumStyle(message),
            ]),
            if (details != null) makeListDetails()
          ],
        ),
      ),
    );
  }
}
