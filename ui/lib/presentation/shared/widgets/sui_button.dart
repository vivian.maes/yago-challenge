import 'package:flutter/material.dart';

import '../app_colors.dart';
import '../styles.dart';
import '../ui_helpers.dart';

class SuiButton extends StatelessWidget {
  const SuiButton.light(
      {Key? key,
      required this.text,
      required this.iconData,
      required this.onPressed})
      : textStyle = Styles.largeStyle,
        backgroundColor = Colors.white,
        radiusSize = UiHelpers.sizeSmall,
        super(key: key);

  // Parameters déclaration
  final String text;
  final IconData iconData;

  // By constructor declartion
  final TextStyle textStyle;
  final Color backgroundColor;
  final double radiusSize;

  // Action
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return OutlinedButton.icon(
        onPressed: onPressed,
        style: OutlinedButton.styleFrom(
          foregroundColor: const Color(AppColors.primary),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(radiusSize),
          ),
          side: const BorderSide(
            color: Color(AppColors.primary),
            width: 2.0,
            style: BorderStyle.solid,
          ),
        ),
        icon: Icon(iconData),
        label: Text(
          text,
          style: textStyle.copyWith(fontWeight: FontWeight.bold),
        ));
  }
}
