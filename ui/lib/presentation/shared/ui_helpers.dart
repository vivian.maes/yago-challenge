import 'package:flutter/material.dart';

class UiHelpers {
  // Font family constant
  static const String fontFamily = 'Mulish';

  // Size definition

  static const double sizeTiny = 5.0;
  static const double sizeSmall = 10.0;
  static const double sizeRegular = 18.0;
  static const double sizeMedium = 25.0;
  static const double sizeLarge = 50.0;

  // Horizontal Spacing
  static const SizedBox horizontalSpaceTiny = SizedBox(width: sizeTiny);
  static const SizedBox horizontalSpaceSmall = SizedBox(width: sizeSmall);
  static const SizedBox horizontalSpaceRegular = SizedBox(width: sizeRegular);
  static const SizedBox horizontalSpaceMedium = SizedBox(width: sizeMedium);
  static const SizedBox horizontalSpaceLarge = SizedBox(width: sizeLarge);

  // Vertical Spacing
  static const SizedBox verticalSpaceTiny = SizedBox(height: sizeTiny);
  static const SizedBox verticalSpaceSmall = SizedBox(height: sizeSmall);
  static const SizedBox verticalSpaceRegular = SizedBox(height: sizeRegular);
  static const SizedBox verticalSpaceMedium = SizedBox(height: sizeMedium);
  static const SizedBox verticalSpaceLarge = SizedBox(height: sizeLarge);

  // Screen Size helpers
  static double screenWidth(BuildContext context) =>
      MediaQuery.of(context).size.width;
  static double screenHeight(BuildContext context) =>
      MediaQuery.of(context).size.height;
}
