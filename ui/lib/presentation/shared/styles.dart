import 'package:flutter/material.dart';

class Styles {
  static const TextStyle tinyStyle = TextStyle(
    fontSize: 10,
    fontWeight: FontWeight.normal,
  );

  static const TextStyle smallStyle = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.normal,
  );

  static const TextStyle regularStyle = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.normal,
  );

  static const TextStyle bodyStyle = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.normal,
  );

  static const TextStyle mediumStyle = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.normal,
  );

  static const TextStyle largeStyle = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.normal,
  );
}
