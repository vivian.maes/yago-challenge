class AppColors {
  static const int primary = 0xFF662255;
  static const int secondary = 0xFF001F5C;
  static const int tertiary = 0x7D5260;

  static const int neutral = 0xFF2C3131;
  static const int backgroud = 0xFFFFFFFF;

  static const int error = 0xFFE57373;

  static primaryGradient(String code) {
    Map<String, int> gradients = {
      '000': 0xff662255,
      'd010': 0xff5c1f4d,
      'd020': 0xff521b44,
      'd030': 0xff47183b,
      'd040': 0xff3d1433,
      'd050': 0xff33112b,
      'd060': 0xff290e22,
      'd070': 0xff1f0a19,
      'd080': 0xff140711,
      'd090': 0xff0a0308,
      'd100': 0xff000000,
      'l010': 0xff753866,
      'l020': 0xff854e77,
      'l030': 0xff946488,
      'l040': 0xffa37a99,
      'l050': 0xffb391aa,
      'l060': 0xffc2a7bb,
      'l070': 0xffd1bdcc,
      'l080': 0xffe0d3dd,
      'l090': 0xfff0e9ee,
      'l100': 0xffffffff
    };

    if (gradients.containsKey(code)) {
      return gradients[code];
    } else {
      return gradients['000'];
    }
  }

  static segondaryGradientCode(String code) {
    Map<String, int> gradients = {
      '000': 0xff662255,
      'd010': 0xff001c53,
      'd020': 0xff00194a,
      'd030': 0xff001640,
      'd040': 0xff001337,
      'd050': 0xff00102e,
      'd060': 0xff000c25,
      'd070': 0xff00091c,
      'd080': 0xff000612,
      'd090': 0xff000309,
      'd100': 0xff000000,
      'l010': 0xff1a356c,
      'l020': 0xff334c7d,
      'l030': 0xff4d628d,
      'l040': 0xff66799d,
      'l050': 0xff808fae,
      'l060': 0xff99a5be,
      'l070': 0xffb3bcce,
      'l080': 0xffccd2de,
      'l090': 0xffe6e9ef,
      'l100': 0xffffffff
    };

    if (gradients.containsKey(code)) {
      return gradients[code];
    } else {
      return gradients['000'];
    }
  }

  static neutralGradient(String code) {
    Map<String, int> gradients = {
      '000': 0xFF662255,
      'd010': 0xff282c2c,
      'd020': 0xff232727,
      'd030': 0xff1f2222,
      'd040': 0xff1a1d1d,
      'd050': 0xff161919,
      'd060': 0xff121414,
      'd070': 0xff0d0f0f,
      'd080': 0xff090a0a,
      'd090': 0xff040505,
      'd100': 0xff000000,
      'l010': 0xff414646,
      'l020': 0xff565a5a,
      'l030': 0xff6b6f6f,
      'l040': 0xff808383,
      'l050': 0xff969898,
      'l060': 0xffabadad,
      'l070': 0xffc0c1c1,
      'l080': 0xffd5d6d6,
      'l090': 0xffeaeaea,
      'l100': 0xffffffff
    };

    if (gradients.containsKey(code)) {
      return gradients[code];
    } else {
      return gradients['000'];
    }
  }
}
