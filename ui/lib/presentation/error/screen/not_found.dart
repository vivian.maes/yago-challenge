import 'package:flutter/material.dart';

import '../../../platform/paltform_scaffold.dart';

class NotFound extends StatelessWidget {
  const NotFound({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const PlatformScaffold(
      child: Center(
        child: Text('Not foud'),
      ),
    );
  }
}
