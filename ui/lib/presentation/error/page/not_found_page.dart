import '../../../platform/platform_page.dart';
import '../screen/not_found.dart';

class NotFoundPage extends PlatformPage {
  const NotFoundPage() : super(child: const NotFound());
}
