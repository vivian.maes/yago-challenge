import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ui/core/cubit/tiers/tiers_cubit.dart';
import 'package:ui/presentation/main/widget/simulation_ui.dart';
import '../../../platform/paltform_scaffold.dart';
import '../../shared/ui_helpers.dart';
import '../widget/tier_ui.dart';

class MainSreen extends StatefulWidget {
  const MainSreen({Key? key}) : super(key: key);

  @override
  State<MainSreen> createState() => _MainSreenState();
}

class _MainSreenState extends State<MainSreen> {
  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      child: Padding(
        padding: const EdgeInsets.only(left: 8.0, right: 8.0),
        child: ListView(
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          children: [
            Column(
              children: [
                SizedBox(
                  height: 170,
                  child: Image.asset('images/logo.png'),
                ),
                UiHelpers.verticalSpaceMedium,
                const TierUi(),
                BlocBuilder<TiersCubit, TiersState>(builder: ((context, state) {
                  if (state is Tiers) {
                    if (state.entity.id != null &&
                        state.entity.nacebelCodes != null) {
                      return SimulationUi(
                          idTier: state.entity.id!,
                          nacebelCodes: state.entity.nacebelCodes!);
                    } else {
                      return UiHelpers.verticalSpaceMedium;
                    }
                  } else {
                    return UiHelpers.verticalSpaceMedium;
                  }
                }))
              ],
            ),
          ],
        ),
      ),
    );
  }
}
