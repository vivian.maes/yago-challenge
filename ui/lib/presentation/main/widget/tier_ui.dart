import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ui/core/cubit/simulation/simulation_cubit.dart';
import 'package:ui/data/model/entity/tier_entity.dart';
import 'package:ui/data/model/request/tier_request.dart';
import 'package:ui/presentation/main/widget/show_tier.dart';
import 'package:ui/presentation/shared/widgets/sui_error_message.dart';

import '../../../core/cubit/tiers/tiers_cubit.dart';
import '../../shared/ui_helpers.dart';
import '../../shared/widgets/sui_button.dart';
import '../../shared/widgets/sui_text.dart';
import 'aquire_email.dart';

class TierUi extends StatefulWidget {
  const TierUi({Key? key}) : super(key: key);

  @override
  State<TierUi> createState() => _TierUiState();
}

class _TierUiState extends State<TierUi> {
  final emailController = TextEditingController();
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final annualRevenueController = TextEditingController(); // ici
  final enterpriseNumberController = TextEditingController();
  final legalNameController = TextEditingController();
  final nacebelCodesController = TextEditingController();
  bool naturalPersonController = false;

  setFieldData(TierEntity tierEntity) {
    emailController.text = tierEntity.email;

    if (tierEntity.firstName != null) {
      firstNameController.text = tierEntity.firstName!;
    }
    if (tierEntity.lastName != null) {
      lastNameController.text = tierEntity.lastName!;
    }
    if (tierEntity.annualRevenue != null) {
      annualRevenueController.text = tierEntity.annualRevenue!.toString();
    }
    if (tierEntity.enterpriseNumber != null) {
      enterpriseNumberController.text = tierEntity.enterpriseNumber!;
    }
    if (tierEntity.legalName != null) {
      legalNameController.text = tierEntity.legalName!;
    }
    if (tierEntity.naturalPerson != null) {
      naturalPersonController = tierEntity.naturalPerson!;
    }
    if (tierEntity.nacebelCodes != null) {
      nacebelCodesController.text = tierEntity.nacebelCodes!.join(",");
    }
  }

  int? converStringInt(String? value) {
    if (value == null || value.isEmpty) {
      return null;
    } else {
      return int.parse(value);
    }
  }

  TierRequest getRequest() {
    return TierRequest(
        email: emailController.text,
        firstName: firstNameController.text,
        lastName: lastNameController.text,
        enterpriseNumber: enterpriseNumberController.text,
        legalName: legalNameController.text,
        annualRevenue: converStringInt(annualRevenueController.text),
        naturalPerson: naturalPersonController,
        nacebelCodes: nacebelCodesController.text.isEmpty
            ? []
            : nacebelCodesController.text.split(","));
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<TiersCubit, TiersState>(builder: (context, state) {
      if (state is TiersUnknown) {
        return AquireEmail(entity: state);
      }

      if (state is TiersUnsaved) {
        return Column(children: [
          Row(children: const [
            SuiText.mediumStyle(
                'Afin de vous servir nous avons besoin de vous connaître.')
          ]),
          UiHelpers.verticalSpaceRegular,
          TextField(
            controller: emailController,
            enabled: false,
            keyboardType: TextInputType.emailAddress,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Email',
            ),
          ),
          UiHelpers.verticalSpaceTiny,
          TextField(
              controller: firstNameController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Quel est votre prénom ?',
              )),
          UiHelpers.verticalSpaceTiny,
          TextField(
              controller: lastNameController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Quel est votre nom ?',
              )),
          UiHelpers.verticalSpaceTiny,
          TextField(
              controller: annualRevenueController,
              keyboardType: TextInputType.number,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Quel est votre revenu annuel ?',
              )),
          UiHelpers.verticalSpaceTiny,
          TextField(
              controller: enterpriseNumberController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Quel est votre numéro d\'entreprise ?',
              )),
          UiHelpers.verticalSpaceTiny,
          TextField(
              controller: legalNameController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Quel est le nom de votre entreprise ?',
              )),
          UiHelpers.verticalSpaceTiny,
          TextFormField(
              controller: nacebelCodesController,
              keyboardType: TextInputType.multiline,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'nace,code,sample',
              )),
          UiHelpers.verticalSpaceTiny,
          Row(
            children: [
              Checkbox(
                  value: naturalPersonController,
                  onChanged: (bool? value) {
                    setState(() {
                      naturalPersonController = value!;
                    });
                  }),
              UiHelpers.horizontalSpaceSmall,
              const SuiText.smallStyle('Personne physique ?'),
            ],
          ),
          if (state.errorMessage.isNotEmpty) UiHelpers.verticalSpaceTiny,
          if (state.errorMessage.isNotEmpty)
            SuiErrorMessage(
                message: state.errorMessage, details: state.errorDetails),
          UiHelpers.verticalSpaceRegular,
          Row(
            children: [
              SuiButton.light(
                iconData: Icons.save,
                text: 'Create my profile',
                onPressed: () {
                  BlocProvider.of<TiersCubit>(context).createTier(getRequest());
                },
              ),
            ],
          ),
        ]);
      }

      if (state is Tiers) return ShowTier(entity: state.entity);

      return const SuiText.largeStyle("Too bad");
    }, listener: (context, state) {
      if (state is TiersUnsaved) setFieldData(state.entity);
      if (state is Tiers) {
        setFieldData(state.entity);
        BlocProvider.of<SimulationCubit>(context).checkExist(state.entity.id);
      }
    });
  }
}
