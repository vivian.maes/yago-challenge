import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../core/cubit/tiers/tiers_cubit.dart';
import '../../shared/ui_helpers.dart';
import '../../shared/widgets/sui_button.dart';
import '../../shared/widgets/sui_text.dart';

class AquireEmail extends StatelessWidget {
  AquireEmail({required TiersUnknown entity, Key? key}) : super(key: key);

  final emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Row(children: const [
        SuiText.mediumStyle(
            'Afin de vous servir nous avons besoin de vous connaître.')
      ]),
      UiHelpers.verticalSpaceRegular,
      TextField(
        controller: emailController,
        decoration: const InputDecoration(
          border: OutlineInputBorder(),
          hintText: 'Email',
        ),
      ),
      UiHelpers.verticalSpaceRegular,
      Row(
        children: [
          SuiButton.light(
            iconData: Icons.login,
            text: 'Let\'s me in',
            onPressed: () {
              BlocProvider.of<TiersCubit>(context)
                  .checkExistTier(emailController.text);
            },
          ),
        ],
      ),
    ]);
  }
}
