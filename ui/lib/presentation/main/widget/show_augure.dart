import 'package:flutter/material.dart';
import 'package:ui/data/model/entity/expert_entity.dart';

import '../../shared/widgets/sui_text.dart';

class ShowAugure extends StatelessWidget {
  const ShowAugure({required this.entity, Key? key}) : super(key: key);

  final ExpertEntity entity;

  makeListAugure() {
    return Column(
        children: entity.augure
            .map(
              (str) =>
                  Flex(direction: Axis.vertical, children: [SuiText.body(str)]),
            )
            .toList());
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          if (entity.augure.isNotEmpty)
            Row(
              children: [
                SuiText.body("Conseil pour votre simulation"),
              ],
            ),
          makeListAugure(),
        ],
      ),
    );
  }
}
