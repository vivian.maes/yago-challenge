import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ui/core/cubit/expert/expert_cubit.dart';
import 'package:ui/data/model/request/expert_request.dart';
import 'package:ui/data/model/request/simulation_request.dart';
import 'package:ui/presentation/shared/widgets/sui_text.dart';

import '../../../core/cubit/simulation/simulation_cubit.dart';
import '../../shared/ui_helpers.dart';
import '../../shared/widgets/sui_button.dart';
import '../../shared/widgets/sui_error_message.dart';
import 'show_augure.dart';

class SimulationUi extends StatefulWidget {
  const SimulationUi(
      {Key? key, required this.idTier, required this.nacebelCodes})
      : super(key: key);

  final String idTier;
  final List<dynamic> nacebelCodes;

  @override
  State<SimulationUi> createState() => _SimulationUiState();
}

class _SimulationUiState extends State<SimulationUi> {
  String coverageCeilingFormulaController = 'small';
  String deductibleFormulaController = 'small';

  SimulationRequest getRequest(tierId) {
    return SimulationRequest(
        coverageCeilingFormula: coverageCeilingFormulaController,
        deductibleFormula: deductibleFormulaController,
        tierId: tierId);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SimulationCubit, SimulationState>(
        builder: (context, state) {
      if (state is SimulationFound) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UiHelpers.verticalSpaceRegular,
            Row(
              children: const [
                SuiText.largeStyle('Votre simulation'),
              ],
            ),
            UiHelpers.verticalSpaceTiny,
            Row(
              children: [
                SuiText.mediumStyle(
                    "  - Plafond : ${state.entity.coverageCeiling}"),
              ],
            ),
            UiHelpers.verticalSpaceTiny,
            Row(
              children: [
                SuiText.mediumStyle(
                    "  - Deduction : ${state.entity.deductible}"),
              ],
            ),
            UiHelpers.verticalSpaceTiny,
            Row(
              children: const [
                SuiText.mediumStyle('Prime'),
              ],
            ),
            UiHelpers.verticalSpaceTiny,
            Row(
              children: [
                SuiText.mediumStyle(
                    "  - Après livraison : ${state.entity.grossPremiums["afterDelivery"]}"),
              ],
            ),
            UiHelpers.verticalSpaceTiny,
            Row(
              children: [
                SuiText.mediumStyle(
                    "  - Objets confiés : ${state.entity.grossPremiums["entrustedObjects"]}"),
              ],
            ),
            UiHelpers.verticalSpaceTiny,
            Row(
              children: [
                SuiText.mediumStyle(
                    "  - Frais juridiques : ${state.entity.grossPremiums["legalExpenses"]}"),
              ],
            ),
            UiHelpers.verticalSpaceTiny,
            Row(
              children: [
                SuiText.mediumStyle(
                    "  - Indemnité professionnelle : ${state.entity.grossPremiums["professionalIndemnity"]}"),
              ],
            ),
            UiHelpers.verticalSpaceTiny,
            Row(
              children: [
                SuiText.mediumStyle(
                    "  - Responsabilité civile : ${state.entity.grossPremiums["publicLiability"]}"),
              ],
            ),
          ],
        );
      } else {
        BlocProvider.of<ExpertCubit>(context)
            .checkExist(ExpertRequest(nacebelCodes: widget.nacebelCodes));
        return Column(
          children: [
            UiHelpers.verticalSpaceLarge,
            Row(
              children: const [
                SuiText.largeStyle('Créer une simulation'),
              ],
            ),
            BlocBuilder<ExpertCubit, ExpertState>(builder: ((context, state) {
              if (state is ExpertFound) {
                return ShowAugure(entity: state.entity);
              }
              return Container();
            })),
            Row(
              children: const [
                SuiText.mediumStyle('Formule de déductibilité'),
              ],
            ),
            DropDownTextField(
              clearOption: false,
              // searchAutofocus: true,
              dropDownItemCount: 8,
              initialValue: "small",
              searchShowCursor: false,
              enableSearch: false,
              searchKeyboardType: TextInputType.number,
              dropDownList: const [
                DropDownValueModel(name: 'small', value: "small"),
                DropDownValueModel(name: 'medium', value: "medium"),
                DropDownValueModel(name: 'large', value: "large"),
              ],
              onChanged: (val) {
                deductibleFormulaController = (val as DropDownValueModel).value;
              },
            ),
            UiHelpers.verticalSpaceMedium,
            Row(
              children: const [
                SuiText.mediumStyle('Plafond de couverture'),
              ],
            ),
            DropDownTextField(
              clearOption: false,
              // searchAutofocus: true,
              dropDownItemCount: 8,
              initialValue: "small",
              searchShowCursor: false,
              enableSearch: false,
              searchKeyboardType: TextInputType.number,
              dropDownList: const [
                DropDownValueModel(name: 'small', value: "small"),
                DropDownValueModel(name: 'large', value: "large"),
              ],
              onChanged: (val) {
                coverageCeilingFormulaController =
                    (val as DropDownValueModel).value;
              },
            ),
            if (state is SimulationUnsaved)
              if (state.errorMessage.isNotEmpty) UiHelpers.verticalSpaceRegular,
            if (state is SimulationUnsaved)
              if (state.errorMessage.isNotEmpty)
                SuiErrorMessage(
                    message: state.errorMessage, details: state.errorDetails),
            UiHelpers.verticalSpaceRegular,
            Row(
              children: [
                SuiButton.light(
                  iconData: Icons.build,
                  text: 'Create my simulation',
                  onPressed: () {
                    BlocProvider.of<SimulationCubit>(context)
                        .createSimulation(getRequest(widget.idTier));
                  },
                ),
              ],
            ),
            UiHelpers.verticalSpaceRegular,
          ],
        );
      }
    }, listener: (context, state) {
      if (state is SimulationUnsaved) {
        BlocProvider.of<ExpertCubit>(context)
            .checkExist(ExpertRequest(nacebelCodes: widget.nacebelCodes));
      }
    });
  }
}
