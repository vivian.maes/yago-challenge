import 'package:flutter/material.dart';
import 'package:ui/data/model/entity/tier_entity.dart';

import '../../shared/ui_helpers.dart';
import '../../shared/widgets/sui_text.dart';

class ShowTier extends StatelessWidget {
  const ShowTier({required this.entity, Key? key}) : super(key: key);

  final TierEntity entity;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        UiHelpers.verticalSpaceLarge,
        Row(
          children: const [
            SuiText.largeStyle('Votre profil'),
          ],
        ),
        if (entity.firstName != null && entity.firstName!.isNotEmpty)
          UiHelpers.verticalSpaceTiny,
        if (entity.firstName != null && entity.firstName!.isNotEmpty)
          Row(
            children: [
              SuiText.mediumStyle("  - Prenom : ${entity.firstName}"),
            ],
          ),
        if (entity.lastName != null && entity.lastName!.isNotEmpty)
          UiHelpers.verticalSpaceTiny,
        if (entity.lastName != null && entity.lastName!.isNotEmpty)
          Row(
            children: [
              SuiText.mediumStyle("  - Nom : ${entity.lastName}"),
            ],
          ),
        UiHelpers.verticalSpaceTiny,
        Row(
          children: [
            SuiText.mediumStyle("  - email : ${entity.email}"),
          ],
        ),
        UiHelpers.verticalSpaceTiny,
        Row(
          children: [
            SuiText.mediumStyle(
                "  - Nom de l'entreprise : ${entity.legalName}"),
          ],
        ),
        UiHelpers.verticalSpaceTiny,
        Row(
          children: [
            SuiText.mediumStyle(
                "  - Numéro d'entreprise : ${entity.enterpriseNumber}"),
          ],
        ),
        if (entity.nacebelCodes != null && entity.nacebelCodes!.isNotEmpty)
          UiHelpers.verticalSpaceTiny,
        if (entity.nacebelCodes != null && entity.nacebelCodes!.isNotEmpty)
          Row(
            children: [
              SuiText.mediumStyle(
                  "  - Code Nacebel : ${entity.nacebelCodes!.join(" - ")}"),
            ],
          ),
        UiHelpers.verticalSpaceTiny,
        Row(
          children: [
            SuiText.mediumStyle("  - Revenu : ${entity.annualRevenue} €"),
          ],
        ),
      ],
    );
  }
}
