import '../../../platform/platform_page.dart';
import '../screen/main_screen.dart';

class MainPage extends PlatformPage {
  const MainPage() : super(child: const MainSreen());
}
