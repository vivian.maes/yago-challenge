import 'package:flutter/material.dart';

import '../presentation/main/page/main_page.dart';

Map<String, Page Function(Map<String, dynamic>)> routesConfiguration = {
  '/': (args) => const MainPage()
};
