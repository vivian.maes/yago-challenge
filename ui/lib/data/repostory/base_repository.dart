class BaseRepository {
  BaseRepository.fromEndPointApi({required this.endPoint});

  final String endPoint;
}
