import 'dart:convert';

import 'package:ui/data/model/response/error_response.dart';
import 'package:ui/data/repostory/base_repository.dart';
import 'package:http/http.dart' as http;

import '../model/request/simulation_request.dart';
import '../model/response/simulation_response.dart';

class SimulationRepository extends BaseRepository {
  SimulationRepository.fromEndPointApi({required super.endPoint})
      : super.fromEndPointApi();

  Future getByTierId(String id) async {
    final response =
        await http.get(Uri.parse("${super.endPoint}/by_tier_id/$id"));

    if (response.statusCode == 200) {
      Map<String, dynamic> json = jsonDecode(response.body);
      if (json.containsKey('error')) {
        return ErrorResponse.fromMap(json);
      } else {
        return SimulationResponse.fromMap(json).toEntity();
      }
    } else {
      return ErrorResponse.fromMap(jsonDecode(response.body));
    }
  }

  Future create(SimulationRequest request) async {
    final response = await http.post(Uri.parse("${super.endPoint}/new"),
        headers: {
          'Content-type': 'application/json',
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Credentials": "true",
          "Access-Control-Allow-Methods": "GET,HEAD,OPTIONS,POST,PUT",
          "Access-Control-Allow-Headers":
              "Origin, X-Requested-With, Content-Type, Accept, Authorization",
        },
        body: jsonEncode(request.toMap()));

    if (response.statusCode == 200) {
      return SimulationResponse.fromMap(jsonDecode(response.body)).toEntity();
    } else {
      return ErrorResponse.fromMap(jsonDecode(response.body));
    }
  }
}
