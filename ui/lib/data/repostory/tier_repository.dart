import 'dart:convert';

import 'package:ui/data/model/request/tier_request.dart';
import 'package:ui/data/model/response/error_response.dart';
import 'package:ui/data/model/response/tier_response.dart';
import 'package:ui/data/repostory/base_repository.dart';
import 'package:http/http.dart' as http;

class TierRepository extends BaseRepository {
  TierRepository.fromEndPointApi({required super.endPoint})
      : super.fromEndPointApi();

  Future getByEmail(String email) async {
    final response =
        await http.get(Uri.parse("${super.endPoint}/by_email/$email"));

    if (response.statusCode == 200) {
      Map<String, dynamic> json = jsonDecode(response.body);
      if (json.containsKey('error')) {
        return ErrorResponse.fromMap(json);
      } else {
        return TierResponse.fromMap(json).toEntity();
      }
    } else {
      return ErrorResponse.fromMap(jsonDecode(response.body));
    }
  }

  Future getById(String id) async {
    final response = await http.get(Uri.parse("${super.endPoint}/by_id/$id"));

    if (response.statusCode == 200) {
      Map<String, dynamic> json = jsonDecode(response.body);
      if (json.containsKey('error')) {
        return ErrorResponse.fromMap(json);
      } else {
        return TierResponse.fromMap(json).toEntity();
      }
    } else {
      return ErrorResponse.fromMap(jsonDecode(response.body));
    }
  }

  Future create(TierRequest request) async {
    final response = await http.post(Uri.parse("${super.endPoint}/new"),
        headers: {
          'Content-type': 'application/json',
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Credentials": "true",
          "Access-Control-Allow-Methods": "GET,HEAD,OPTIONS,POST,PUT",
          "Access-Control-Allow-Headers":
              "Origin, X-Requested-With, Content-Type, Accept, Authorization",
        },
        body: jsonEncode(request.toMap()));

    if (response.statusCode == 200) {
      return TierResponse.fromMap(jsonDecode(response.body)).toEntity();
    } else {
      return ErrorResponse.fromMap(jsonDecode(response.body));
    }
  }
}
