import 'dart:convert';

import 'package:ui/data/model/response/error_response.dart';

import 'package:ui/data/repostory/base_repository.dart';
import 'package:http/http.dart' as http;

import '../model/request/expert_request.dart';
import '../model/response/expert_response.dart';

class ExpertRepository extends BaseRepository {
  ExpertRepository.fromEndPointApi({required super.endPoint})
      : super.fromEndPointApi();

  Future checkExist(ExpertRequest request) async {
    final response = await http.post(Uri.parse("${super.endPoint}/augure"),
        headers: {
          'Content-type': 'application/json',
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Credentials": "true",
          "Access-Control-Allow-Methods": "GET,HEAD,OPTIONS,POST,PUT",
          "Access-Control-Allow-Headers":
              "Origin, X-Requested-With, Content-Type, Accept, Authorization",
        },
        body: jsonEncode(request.toMap()));

    if (response.statusCode == 200) {
      return ExpertResponse.fromMap(jsonDecode(response.body)).toEntity();
    } else {
      return ErrorResponse.fromMap(jsonDecode(response.body));
    }
  }
}
