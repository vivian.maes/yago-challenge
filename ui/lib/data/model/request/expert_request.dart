import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
class ExpertRequest extends Equatable {
  const ExpertRequest({
    required this.nacebelCodes,
  });

  final List<dynamic> nacebelCodes;

  @override
  List<Object?> get props => [nacebelCodes];

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {};

    map['nacebelCodes'] = nacebelCodes;

    return map;
  }
}
