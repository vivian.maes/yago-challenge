import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
class TierRequest extends Equatable {
  const TierRequest({
    required this.email,
    this.id,
    this.firstName,
    this.lastName,
    this.annualRevenue,
    this.enterpriseNumber,
    this.legalName,
    this.naturalPerson,
    this.nacebelCodes,
  });

  final String email;

  final String? id;
  final String? firstName;
  final String? lastName;
  final int? annualRevenue;
  final String? enterpriseNumber;
  final String? legalName;
  final bool? naturalPerson;
  final List<String>? nacebelCodes;

  TierRequest copyWith(
      {String? email,
      String? id,
      String? firstName,
      String? lastName,
      int? annualRevenue,
      String? enterpriseNumber,
      String? legalName,
      bool? naturalPerson,
      List<String>? nacebelCodes}) {
    return TierRequest(
        email: email ?? this.email,
        id: id ?? this.id,
        firstName: firstName ?? this.firstName,
        lastName: lastName ?? this.lastName,
        annualRevenue: annualRevenue ?? this.annualRevenue,
        enterpriseNumber: enterpriseNumber ?? this.enterpriseNumber,
        legalName: legalName ?? this.legalName,
        naturalPerson: naturalPerson ?? this.naturalPerson,
        nacebelCodes: nacebelCodes ?? this.nacebelCodes);
  }

  @override
  List<Object?> get props => [
        email,
        id,
        firstName,
        lastName,
        annualRevenue,
        enterpriseNumber,
        legalName,
        naturalPerson,
        nacebelCodes
      ];

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {'email': email};

    if (id != null) map['id'] = id;
    if (annualRevenue != null) map['annualRevenue'] = annualRevenue;
    if (naturalPerson != null) map['naturalPerson'] = naturalPerson;
    if (firstName != null && firstName!.isNotEmpty) {
      map['firstName'] = firstName;
    }

    if (lastName != null && lastName!.isNotEmpty) {
      map['lastName'] = lastName;
    }

    if (enterpriseNumber != null && enterpriseNumber!.isNotEmpty) {
      map['enterpriseNumber'] = enterpriseNumber;
    }

    if (legalName != null && legalName!.isNotEmpty) {
      map['legalName'] = legalName;
    }

    if (nacebelCodes != null && nacebelCodes!.isNotEmpty) {
      map['nacebelCodes'] = nacebelCodes;
    }

    return map;
  }
}
