import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
class SimulationRequest extends Equatable {
  const SimulationRequest({
    required this.tierId,
    required this.deductibleFormula,
    required this.coverageCeilingFormula,
  });

  final String tierId;
  final String deductibleFormula;
  final String coverageCeilingFormula;

  @override
  List<Object?> get props =>
      [tierId, deductibleFormula, coverageCeilingFormula];

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {};

    map['tierId'] = tierId;
    map['deductibleFormula'] = deductibleFormula;
    map['coverageCeilingFormula'] = coverageCeilingFormula;

    return map;
  }
}
