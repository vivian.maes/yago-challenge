import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

@immutable
class ErrorResponse extends Equatable {
  const ErrorResponse({required this.message, this.details});

  final String message;
  final List<String>? details;

  @override
  List<Object?> get props => [message];

  factory ErrorResponse.fromMap(Map<String, dynamic> map) {
    List<String>? det;

    if (map['details'] != null) {
      det = [];
      for (var element in (map['details'] as List<dynamic>)) {
        det.add("${element[0]} : ${element[1][0]}");
      }
    }

    return ErrorResponse(
      message: map['error'],
      details: det,
    );
  }
}
