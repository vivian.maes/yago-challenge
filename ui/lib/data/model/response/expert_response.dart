import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import '../entity/expert_entity.dart';

@immutable
class ExpertResponse extends Equatable {
  const ExpertResponse({
    required this.augure,
  });

  final List<dynamic> augure;

  @override
  List<Object?> get props => [augure];

  ExpertEntity toEntity() {
    return ExpertEntity(
      augure: augure,
    );
  }

  factory ExpertResponse.fromMap(Map<String, dynamic> map) {
    return ExpertResponse(
      augure: map['augure'],
    );
  }
}
