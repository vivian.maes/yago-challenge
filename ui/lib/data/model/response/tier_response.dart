import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:ui/data/model/entity/tier_entity.dart';

@immutable
class TierResponse extends Equatable {
  const TierResponse({
    required this.email,
    this.id,
    this.firstName,
    this.lastName,
    this.annualRevenue,
    this.enterpriseNumber,
    this.legalName,
    this.naturalPerson,
    this.nacebelCodes,
  });

  final String email;

  final String? id;
  final String? firstName;
  final String? lastName;
  final int? annualRevenue;
  final String? enterpriseNumber;
  final String? legalName;
  final bool? naturalPerson;
  final List<dynamic>? nacebelCodes;

  @override
  List<Object?> get props => [
        email,
        id,
        firstName,
        lastName,
        annualRevenue,
        enterpriseNumber,
        legalName,
        naturalPerson,
        nacebelCodes
      ];

  TierEntity toEntity() {
    return TierEntity(
      email: email,
      id: id,
      firstName: firstName,
      lastName: lastName,
      annualRevenue: annualRevenue,
      enterpriseNumber: enterpriseNumber,
      legalName: legalName,
      naturalPerson: naturalPerson,
      nacebelCodes: nacebelCodes,
    );
  }

  factory TierResponse.fromMap(Map<String, dynamic> map) {
    return TierResponse(
      email: map['email'],
      id: map['id'],
      firstName: map['firstName'],
      lastName: map['lastName'],
      annualRevenue: map['annualRevenue'],
      enterpriseNumber: map['enterpriseNumber'],
      legalName: map['legalName'],
      naturalPerson: map['naturalPerson'],
      nacebelCodes: map['nacebelCodes'],
    );
  }
}
