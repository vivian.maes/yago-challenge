import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import '../entity/simulation_entity.dart';

@immutable
class SimulationResponse extends Equatable {
  const SimulationResponse({
    required this.id,
    required this.tierId,
    required this.available,
    //required this.annualRevenue,
    required this.coverageCeiling,
    required this.deductible,
    required this.grossPremiums,
    required this.quoteId,
  });

  final String id;
  final String tierId;
  final bool available;
  //final int annualRevenue;
  final int coverageCeiling;
  final int deductible;
  final Map<String, double> grossPremiums;
  final String quoteId;

  @override
  List<Object?> get props => [
        id,
        tierId,
        available,
        // annualRevenue,
        coverageCeiling,
        deductible,
        grossPremiums,
        quoteId
      ];

  SimulationEntity toEntity() {
    return SimulationEntity(
      id: id,
      tierId: tierId,
      available: available,
      //  annualRevenue: annualRevenue,
      coverageCeiling: coverageCeiling,
      deductible: deductible,
      grossPremiums: grossPremiums,
      quoteId: quoteId,
    );
  }

  factory SimulationResponse.fromMap(Map<String, dynamic> map) {
    Map<String, double> grossPremiums = {};
    grossPremiums["afterDelivery"] = map['grossPremiums']["afterDelivery"];
    grossPremiums["entrustedObjects"] =
        map['grossPremiums']["entrustedObjects"];
    grossPremiums["legalExpenses"] = map['grossPremiums']["legalExpenses"];
    grossPremiums["professionalIndemnity"] =
        map['grossPremiums']["professionalIndemnity"];
    grossPremiums["publicLiability"] = map['grossPremiums']["publicLiability"];

    return SimulationResponse(
      id: map['id'],
      tierId: map['tierId'],
      available: map['available'],
      // annualRevenue: map['annualRevenue'],
      coverageCeiling: map['coverageCeiling'],
      deductible: map['deductible'],
      grossPremiums: grossPremiums,
      quoteId: map['quoteId'],
    );
  }
}
