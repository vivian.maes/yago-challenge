import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

@immutable
class SimulationEntity extends Equatable {
  const SimulationEntity({
    required this.id,
    required this.tierId,
    required this.available,
    //required this.annualRevenue,
    required this.coverageCeiling,
    required this.deductible,
    required this.grossPremiums,
    required this.quoteId,
  });

  final String id;
  final String tierId;
  final bool available;
  //final int annualRevenue;
  final int coverageCeiling;
  final int deductible;
  final Map<String, double> grossPremiums;
  final String quoteId;

  @override
  List<Object?> get props => [
        id,
        tierId,
        available,
        //    annualRevenue,
        coverageCeiling,
        deductible,
        grossPremiums,
        quoteId
      ];
}
