import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

@immutable
class TierEntity extends Equatable {
  const TierEntity({
    required this.email,
    this.id,
    this.firstName,
    this.lastName,
    this.annualRevenue,
    this.enterpriseNumber,
    this.legalName,
    this.naturalPerson,
    this.nacebelCodes,
  });

  final String email;

  final String? id;
  final String? firstName;
  final String? lastName;
  final int? annualRevenue;
  final String? enterpriseNumber;
  final String? legalName;
  final bool? naturalPerson;
  final List<dynamic>? nacebelCodes;

  TierEntity copyWith(
      {String? email,
      String? id,
      String? firstName,
      String? lastName,
      int? annualRevenue,
      String? enterpriseNumber,
      String? legalName,
      bool? naturalPerson,
      List<dynamic>? nacebelCodes}) {
    return TierEntity(
        email: email ?? this.email,
        id: id ?? this.id,
        firstName: firstName ?? this.firstName,
        lastName: lastName ?? this.lastName,
        annualRevenue: annualRevenue ?? this.annualRevenue,
        enterpriseNumber: enterpriseNumber ?? this.enterpriseNumber,
        legalName: legalName ?? this.legalName,
        naturalPerson: naturalPerson ?? this.naturalPerson,
        nacebelCodes: nacebelCodes ?? this.nacebelCodes);
  }

  @override
  List<Object?> get props => [
        email,
        id,
        firstName,
        lastName,
        annualRevenue,
        enterpriseNumber,
        legalName,
        naturalPerson,
        nacebelCodes
      ];
}
