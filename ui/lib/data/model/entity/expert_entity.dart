import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

@immutable
class ExpertEntity extends Equatable {
  const ExpertEntity({
    required this.augure,
  });

  final List<dynamic> augure;

  @override
  List<Object?> get props => [augure];
}
