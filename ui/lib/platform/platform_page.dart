import 'package:flutter/material.dart';

class PlatformPage extends Page {
  final Widget child;

  const PlatformPage({Key? key, required this.child});

  @override
  Route createRoute(BuildContext context) {
    return MaterialPageRoute(
      settings: this,
      builder: (_) => child,
    );
  }
}
