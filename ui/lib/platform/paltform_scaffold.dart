import 'package:flutter/material.dart';

class PlatformScaffold extends StatelessWidget {
  final Widget child;
  final AppBar? materialAppBar;

  const PlatformScaffold({
    Key? key,
    this.materialAppBar,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: materialAppBar,
        body: child,
      ),
    );
  }
}
