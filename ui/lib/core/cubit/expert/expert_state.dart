part of 'expert_cubit.dart';

abstract class ExpertState extends Equatable {
  const ExpertState();

  @override
  List<Object> get props => [];
}

class ExpertUnknown extends ExpertState {}

class ExpertFound extends ExpertState {
  final ExpertEntity entity;
  const ExpertFound({required this.entity});

  @override
  List<Object> get props => [entity];
}
