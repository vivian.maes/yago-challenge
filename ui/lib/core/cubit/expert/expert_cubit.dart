import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/model/entity/expert_entity.dart';
import '../../../data/model/request/expert_request.dart';
import '../../../data/repostory/expert_repository.dart';

part 'expert_state.dart';

class ExpertCubit extends Cubit<ExpertState> {
  final ExpertRepository repository;
  ExpertCubit({required BuildContext context, required this.repository})
      : super(ExpertUnknown());

  Future checkExist(ExpertRequest req) async {
    dynamic entity = await repository.checkExist(req);
    if (entity is ExpertEntity) {
      emit(ExpertFound(entity: entity));
    }
  }
}
