part of 'tiers_cubit.dart';

abstract class TiersState extends Equatable {
  const TiersState();

  @override
  List<Object> get props => [];
}

class TiersUnknown extends TiersState {}

class Tiers extends TiersState {
  final TierEntity entity;

  const Tiers({required this.entity});

  @override
  List<Object> get props => [entity];
}

class TiersUnsaved extends TiersState {
  final TierEntity entity;
  final String errorMessage;
  final List<String> errorDetails;
  const TiersUnsaved(
      {required this.entity,
      required this.errorMessage,
      required this.errorDetails});

  @override
  List<Object> get props => [entity, errorMessage, errorDetails];
}
