import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ui/data/model/entity/tier_entity.dart';
import 'package:ui/data/model/request/tier_request.dart';
import 'package:ui/data/model/response/error_response.dart';
import 'package:ui/data/repostory/tier_repository.dart';

part 'tiers_state.dart';

class TiersCubit extends Cubit<TiersState> {
  final TierRepository repository;
  TiersCubit({required BuildContext context, required this.repository})
      : super(TiersUnknown());

  Future checkExistTier(String email) async {
    if (email != "") {
      dynamic entity = await repository.getByEmail(email);
      if (entity is TierEntity) {
        emit(Tiers(entity: entity));
      } else {
        if ((entity as ErrorResponse).message == "Tier does not exist") {
          emit(TiersUnsaved(
              entity: TierEntity(email: email),
              errorMessage: "",
              errorDetails: const []));
        }
      }
    }
  }

  Future createTier(TierRequest request) async {
    dynamic entity = await repository.create(request);
    if (entity is TierEntity) {
      emit(Tiers(entity: entity));
    }
    if (entity is ErrorResponse) {
      emit(TiersUnsaved(
          entity: (state as TiersUnsaved).entity,
          errorMessage: entity.message,
          errorDetails: entity.details ?? []));
    }
  }
}
