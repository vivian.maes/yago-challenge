import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ui/data/model/request/simulation_request.dart';

import '../../../data/model/entity/simulation_entity.dart';
import '../../../data/model/response/error_response.dart';
import '../../../data/repostory/simulation_repository.dart';

part 'simulation_state.dart';

class SimulationCubit extends Cubit<SimulationState> {
  final SimulationRepository repository;
  SimulationCubit({required BuildContext context, required this.repository})
      : super(SimulationUnknown());

  Future checkExist(String? id) async {
    if (id != "" && id != null) {
      dynamic entity = await repository.getByTierId(id);
      if (entity is SimulationEntity) {
        emit(SimulationFound(entity: entity));
      } else {
        if ((entity as ErrorResponse).message == "Simulation does not exist") {
          emit(SimulationUnsaved(
              tierId: id, errorDetails: const [], errorMessage: ""));
        }
      }
    }
  }

  Future createSimulation(SimulationRequest request) async {
    dynamic entity = await repository.create(request);
    if (entity is SimulationEntity) {
      emit(SimulationFound(entity: entity));
    }

    if (entity is ErrorResponse) {
      emit(SimulationUnsaved(
          errorMessage: entity.message,
          errorDetails: entity.details ?? [],
          tierId: (state as SimulationUnsaved).tierId));
    }
  }
}
