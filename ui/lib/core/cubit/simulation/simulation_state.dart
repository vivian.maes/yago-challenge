part of 'simulation_cubit.dart';

abstract class SimulationState extends Equatable {
  const SimulationState();

  @override
  List<Object> get props => [];
}

class SimulationUnknown extends SimulationState {}

class SimulationFound extends SimulationState {
  final SimulationEntity entity;
  const SimulationFound({required this.entity});

  @override
  List<Object> get props => [entity];
}

class SimulationUnsaved extends SimulationState {
  final String tierId;
  final String errorMessage;
  final List<String> errorDetails;
  const SimulationUnsaved(
      {required this.tierId,
      required this.errorMessage,
      required this.errorDetails});

  @override
  List<Object> get props => [tierId, errorMessage, errorDetails];
}
