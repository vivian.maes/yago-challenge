import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'navigation_cubit.dart';
import 'navigation_stack.dart';
import 'ser_route_configuration.dart';

class SerRouterDelegate extends RouterDelegate<SerRouteConfiguration>
    with
        ChangeNotifier,
        PopNavigatorRouterDelegateMixin<SerRouteConfiguration> {
  final NavigationCubit navigationCubit;

  SerRouterDelegate(this.navigationCubit);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<NavigationCubit, NavigationStack>(
      builder: (context, stack) => Navigator(
        pages: stack.pages,
        key: navigatorKey,
        onPopPage: (route, result) => _onPopPage.call(route, result),
      ),
      listener: (context, stack) {},
    );
  }

  ///here we handle back buttons
  bool _onPopPage(Route<dynamic> route, dynamic result) {
    final didPop = route.didPop(result);
    if (!didPop) {
      return false;
    }
    if (navigationCubit.canPop()) {
      navigationCubit.pop();
      return true;
    } else {
      return false;
    }
  }

  @override
  Future<void> setNewRoutePath(SerRouteConfiguration configuration) async {
    if (configuration.route != '/') {
      navigationCubit.push(configuration.route, configuration.args);
    }
  }

  ///called by router when it detects it may have changed because of a rebuild
  ///necessary for backward and forward buttons to work properly
  @override
  SerRouteConfiguration? get currentConfiguration => navigationCubit.state.last;

  @override
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey();
}
