import '../../platform/platform_page.dart';
import 'ser_route_configuration.dart';

class NavigationStack {
  final List<SerRouteConfiguration> srcStack;

  NavigationStack(this.srcStack);

  // ??????
  // List<SerPage> get pages => List.unmodifiable(srcStack.map((e) => e.page));
  List<PlatformPage> get pages =>
      List.unmodifiable(srcStack.map((e) => e.page()));

  List<SerRouteConfiguration> get configs => srcStack;
  int get length => srcStack.length;
  SerRouteConfiguration get first => srcStack.first;
  SerRouteConfiguration get last => srcStack.last;

  ///the reason behind returning Navigation Stack instead of just being a void
  ///is to chain calls as we'll see in navigation_cubit.dart
  //not to go into how a cubit defines a state to be new with lists, I've just returned a new instance

  //pages list in navigator can't be empty, remember
  void clear() {
    srcStack.removeRange(0, srcStack.length - 2);
  }

  bool canPop() {
    return srcStack.length > 1;
  }

  NavigationStack pop() {
    if (canPop()) srcStack.remove(srcStack.last);
    return NavigationStack(srcStack);
  }

  NavigationStack pushBeneathCurrent(SerRouteConfiguration config) {
    srcStack.insert(srcStack.length - 1, config);
    return NavigationStack(srcStack);
  }

  NavigationStack push(SerRouteConfiguration config) {
    if (srcStack.last != config) srcStack.add(config);
    return NavigationStack(srcStack);
  }

  NavigationStack replace(SerRouteConfiguration config) {
    if (canPop()) {
      srcStack.removeLast();
      push(config);
    } else {
      srcStack.insert(0, config);
      srcStack.removeLast();
    }
    return NavigationStack(srcStack);
  }

  NavigationStack clearAndPush(SerRouteConfiguration config) {
    srcStack.clear();
    srcStack.add(config);
    return NavigationStack(srcStack);
  }

  NavigationStack clearAndPushAll(List<SerRouteConfiguration> configs) {
    srcStack.clear();
    srcStack.addAll(configs);
    return NavigationStack(srcStack);
  }
}
