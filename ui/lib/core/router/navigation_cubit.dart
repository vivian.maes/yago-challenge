import 'package:flutter_bloc/flutter_bloc.dart';

import 'navigation_stack.dart';
import 'ser_route_configuration.dart';

class NavigationCubit extends Cubit<NavigationStack> {
  NavigationCubit(List<SerRouteConfiguration> initialPages)
      : super(NavigationStack(initialPages));

  void push(String path, [Map<String, dynamic>? args]) {
    SerRouteConfiguration config =
        SerRouteConfiguration(location: path, args: args);
    emit(state.push(config));
  }

  void clearAndPush(String path, [Map<String, dynamic>? args]) {
    SerRouteConfiguration config =
        SerRouteConfiguration(location: path, args: args);
    emit(state.clearAndPush(config));
  }

  void pop() {
    emit(state.pop());
  }

  bool canPop() {
    return state.canPop();
  }

  void pushBeneathCurrent(String path, [Map<String, dynamic>? args]) {
    final SerRouteConfiguration config =
        SerRouteConfiguration(location: path, args: args);
    emit(state.pushBeneathCurrent(config));
  }
}
