import 'package:flutter/material.dart';

import 'ser_route_configuration.dart';

class SerRouteInformationParser
    extends RouteInformationParser<SerRouteConfiguration> {
  ///get a location (path) from the system and build your route wrapping object
  @override
  Future<SerRouteConfiguration> parseRouteInformation(
      RouteInformation routeInformation) async {
    Uri uri = Uri.parse(routeInformation.location ?? '/');

    Map<String, dynamic> qp = uri.queryParameters;
    String path = uri.path;

    SerRouteConfiguration config =
        SerRouteConfiguration(location: path, args: qp);
    return config;
  }

  ///update the URL bar with the latest URL from the app
  @override
  RouteInformation? restoreRouteInformation(
      SerRouteConfiguration configuration) {
    return RouteInformation(location: configuration.path.toString());
  }
}
