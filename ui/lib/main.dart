import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ui/core/cubit/tiers/tiers_cubit.dart';
import 'package:ui/data/repostory/tier_repository.dart';
import 'package:ui/presentation/theme/purple_theme.dart';

import 'core/cubit/expert/expert_cubit.dart';
import 'core/cubit/simulation/simulation_cubit.dart';
import 'core/router/navigation_cubit.dart';
import 'core/router/ser_route_configuration.dart';
import 'core/router/ser_route_information_parser.dart';
import 'core/router/ser_router_delegate.dart';
import 'data/repostory/expert_repository.dart';
import 'data/repostory/simulation_repository.dart';

void main() {
  runApp(YaGoChalengeApp());
}

class YaGoChalengeApp extends StatelessWidget {
  YaGoChalengeApp({super.key});

  final navigationCubit =
      NavigationCubit([SerRouteConfiguration(location: '/')]);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
        providers: [
          RepositoryProvider<TierRepository>(
              create: (context) => TierRepository.fromEndPointApi(
                  endPoint: "http://yago.meos.cloud/tier_service")),
          RepositoryProvider<SimulationRepository>(
              create: (context) => SimulationRepository.fromEndPointApi(
                  endPoint: "http://yago.meos.cloud/simulation_service")),
          RepositoryProvider<ExpertRepository>(
              create: (context) => ExpertRepository.fromEndPointApi(
                  endPoint: "http://yago.meos.cloud/expert_service")),
        ],
        child: MultiBlocProvider(
            providers: [
              BlocProvider<NavigationCubit>.value(value: navigationCubit),
              BlocProvider<TiersCubit>(
                  create: (context) => TiersCubit(
                      context: context,
                      repository:
                          RepositoryProvider.of<TierRepository>(context))),
              BlocProvider<SimulationCubit>(
                  create: (context) => SimulationCubit(
                      context: context,
                      repository: RepositoryProvider.of<SimulationRepository>(
                          context))),
              BlocProvider<ExpertCubit>(
                  create: (context) => ExpertCubit(
                      context: context,
                      repository:
                          RepositoryProvider.of<ExpertRepository>(context))),
            ],
            child: BlocBuilder(
                bloc: navigationCubit,
                builder: (context, _) {
                  return MaterialApp.router(
                    title: 'YaGo Backend Chalenge',
                    debugShowCheckedModeBanner: false,
                    theme: purpleTheme,
                    routerDelegate: SerRouterDelegate(
                        BlocProvider.of<NavigationCubit>(context)),
                    routeInformationParser: SerRouteInformationParser(),
                  );
                })));
  }
}
