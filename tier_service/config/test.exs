import Config

config :tier_service, port: 80

config :tier_service, database: "tiers_db"
config :tier_service, database_host: "tiedbr_db"
config :tier_service, pool_size: 3
