defmodule TierService do
  @moduledoc """
  Documentation for `TierService`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> TierService.hello()
      :world

  """
  def hello do
    :world
  end
end
