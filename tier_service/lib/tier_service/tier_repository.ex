defmodule TierService.Repository do

  # The will represent our document's name.
  @collection "Tiers"

  def list() do
    :mongo |> Mongo.find(@collection, %{})
  end

  def get_by_id(id) do
    :mongo |> Mongo.find_one(@collection, %{"_id" => id})
  end

  def get_by_email(email) do
    :mongo |> Mongo.find_one(@collection, %{"email" => email})
  end

  def create(data) do
    :mongo |> Mongo.insert_one(@collection, data)
  end

  def update(id, data) do
    :mongo |> Mongo.find_one_and_update(@collection, %{"_id" => id}, %{"$set": data})
  end

  def delete(id) do
    :mongo |> Mongo.find_one_and_delete(@collection, %{"_id" => id})
  end

end
