defmodule TierService.Schema do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key  {:id, :binary_id, autogenerate: true}

  schema "tier" do
    field :email, :string
    field :firstName, :string
    field :lastName, :string
    field :annualRevenue, :integer
    field :enterpriseNumber, :string
    field :legalName, :string
    field :naturalPerson, :boolean
    field :nacebelCodes, {:array, :string}
  end

  def changeset(params) do
    %__MODULE__{}
    |> cast(params, [
      :email,:firstName,:lastName,:annualRevenue,:enterpriseNumber,:legalName,:naturalPerson,:nacebelCodes
    ])
    |> validate_required([:email, :nacebelCodes, :enterpriseNumber, :legalName, :naturalPerson,:annualRevenue])
    |> validate_format(:email, ~r/@/)
    |> validate_length(:enterpriseNumber, is: 10)
    |> validate_length(:nacebelCodes, min: 1)
    |> Map.take([:changes, :errors, :valid?])
  end

  def updateset(params) do
    %__MODULE__{}
    |> cast(params, [
      :firstName,:lastName,:annualRevenue,:enterpriseNumber,:legalName,:naturalPerson,:nacebelCodes
    ])
    |> validate_format(:email, ~r/@/)
    |> validate_length(:enterpriseNumber, is: 10)
    |> validate_length(:nacebelCodes, min: 1)
    |> Map.take([:changes, :errors, :valid?])
  end
end
