defmodule TierService.JsonTOOLS do
  defimpl Jason.Encoder, for: BSON.ObjectId do
    def encode(id, options) do
      BSON.ObjectId.encode!(id)
      |> Jason.Encoder.encode(options)
    end
  end

  defimpl Jason.Encoder, for: Tuple do
    def encode(data, options) when is_tuple(data) do
      data
      |> Tuple.to_list()
      |> Jason.Encoder.List.encode(options)
    end
  end

  @spec normaliseMongoId(map) :: map
  def normaliseMongoId(doc) do
    doc
    |> Map.put('id', doc["_id"])
    |> Map.delete("_id")
  end

  def noEmail(doc) do
    doc
    |> Map.delete("email")
  end
end
