defmodule TierService.Controller do
  alias TierService.JsonTOOLS, as: JSON

  require Logger

  def get_all() do
    data = TierService.Repository.list()
      |> Enum.map(&JSON.normaliseMongoId/1)
      |> Enum.to_list()
      |> Jason.encode!()
    {:ok, data}
  end

  def get_by_id(id) when is_binary(id) do
    try do
      data = BSON.ObjectId.decode!(id)
      |> TierService.Repository.get_by_id()
      |> JSON.normaliseMongoId()
      |> Jason.encode!()

      {:ok, data}
    rescue
      _ in FunctionClauseError -> {:not_found, "Tier does not exist"}
      _ -> {:server_error, "An error occurred internally"}
    end
  end
  def get_by_id(_), do: {:not_found, "Tier does not exist"}

  def get_by_email(email) do
    try do
      case TierService.Repository.get_by_email(email) do
        nil ->
          {:not_found, "Tier does not exist"}
        raw ->
          data = JSON.normaliseMongoId(raw)
          |> Jason.encode!()
          {:ok, data}
      end
    rescue
      _ in FunctionClauseError -> {:not_found, "Tier does not exist"}
      _ -> {:server_error, "An error occurred internally"}
    end
  end

  def create(data) do
    try do
      case TierService.Schema.changeset(data) do
        %{changes: changes, errors: _, valid?: true} ->
        case TierService.Repository.get_by_email(changes.email) do
          nil ->
            {:ok, new_tier} = TierService.Repository.create(changes)
            doc = TierService.Repository.get_by_id(new_tier.inserted_id)
            data =
              JSON.normaliseMongoId(doc)
              |> Jason.encode!()
            {:ok, data}
          _ -> {:malformed_data, "Duplicate found"}
        end
        %{changes: _, errors: err, valid?: false} -> {:malformed_data, err}
      end
    rescue
      _ -> {:server_error, "An error occurred internally"}
    end
  end

  def update(id, params) when is_binary(id) do
    try do
      idb = BSON.ObjectId.decode!(id)
      case TierService.Repository.get_by_id(idb) do
        nil -> {:not_found, "Tier does not exist"}
        _ ->
          case TierService.Schema.updateset(params) do
            %{changes: changes, errors: _, valid?: true} ->
              {:ok, _} = TierService.Repository.update(idb, changes |> JSON.noEmail)
              doc = TierService.Repository.get_by_id(idb)
              data = doc
              |> JSON.normaliseMongoId()
              |> Jason.encode!()
              {:ok, data}
            %{changes: _, errors: _, valid?: false} -> {:malformed_data, "Unprocessable Entity"}
          end
      end
    rescue
        _ in FunctionClauseError -> {:not_found, "Tier does not exist"}
        _ -> {:server_error, "An error occurred internally"}
    end
  end
  def update(_, _), do: {:not_found, "Tier does not exist"}

end
