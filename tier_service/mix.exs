defmodule TierService.MixProject do
  use Mix.Project

  def project do
    [
      app: :tier_service,
      version: "0.1.0",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {TierService.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:mongodb_driver, "~> 0.9.1"},
      {:phoenix_ecto, "~> 4.1"},
      {:plug_cowboy, "~> 2.5"},
      {:jason, "~> 1.4"}
    ]
  end
end
