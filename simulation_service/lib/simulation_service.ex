defmodule SimulationService do
  @moduledoc """
  Documentation for `SimulationService`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> SimulationService.hello()
      :world

  """
  def hello do
    :world
  end
end
