defmodule SimulationService.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application
  require Logger

  @impl true
  def start(_type, _args) do
    children = [
      {
        Plug.Cowboy,
        scheme: :http,
        plug: SimulationService.Router,
        options: [port: Application.get_env(:simulation_service, :port)]
      },{
        Mongo,
        [
          name: :mongo,
          url: "mongodb://#{Application.get_env(:simulation_service, :database_host)}:27017/#{Application.get_env(:simulation_service, :database)}",
          pool_size: Application.get_env(:simulation_service, :pool_size)
        ]
      }
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: SimulationService.Supervisor]
    # Logger.info "Le service de tiers ecoute sur le port: #{port()}..."
    Supervisor.start_link(children, opts)
  end
end
