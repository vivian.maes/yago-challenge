defmodule SimulationService.QuoteRepository do
  require HTTPoison
  require Logger

  def make_one(params) do
    headers = ["Content-Type": "application/json"]
    {:ok, response} = HTTPoison.post("#{Application.get_env(:simulation_service, :quote_service_url)}/quote",params|>Jason.encode!() , headers)
    {:ok, bodyMap} = Jason.decode(response.body)
    %{status_code: response.status_code, body: bodyMap}
  end
end
