defmodule SimulationService.Router do
  use Plug.Router
  require Logger

  plug(:match)
  plug(Plug.Parsers,
    parsers: [:json],
    pass: ["application/json"],
    json_decoder: Jason
  )
  plug(:dispatch)

  get "/" do
    send_resp(conn, 200, "OK")
  end

  get "/health" do
    case Mongo.command(:mongo, ping: 1) do
      {:ok, _res} -> send_resp(conn, 200, "Who's there?")
      {:error, _err} -> send_resp(conn, 500, "Something went wrong")
    end
  end

  get "/all" do
    SimulationService.Controller.get_all()
    |> handle_response(conn)
  end

  get "/by_id/:id" do
    SimulationService.Controller.get_by_id(id)
    |> handle_response(conn)
  end

  get "/by_tier_id/:id" do
    SimulationService.Controller.get_by_ref_id(id)
    |> handle_response(conn)
  end

  post "/new" do
    SimulationService.Controller.create(conn.body_params)
    |> handle_response(conn)
  end

  match _ do
    send_resp(conn, 404, "Not Found")
  end

  defp handle_response(response, conn) do
    %{code: code, message: message} =
      case response do
        {:ok, message} -> %{code: 200, message: message}
        {:not_found, message} -> %{code: 422, message: Jason.encode!(%{error: message})}
        {:malformed_data, message} -> %{code: 422, message: Jason.encode!(%{error: message})}
        {:server_error, _} -> %{code: 500, message: Jason.encode!(%{error: "An error occurred internally"})}
      end

    conn
    |> put_resp_content_type("application/json")
    |> put_resp_header("Access-Control-Allow-Method", "GET,HEAD,OPTIONS,POST,PUT")
    |> put_resp_header("Access-Control-Allow-Origin", "*")
    |> put_resp_header("Access-Control-Allow-Credentials", "true")
    |> put_resp_header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
    |> send_resp(code, message)
  end

end
