defmodule SimulationService.Controller do
  alias SimulationService.JsonTOOLS, as: JSON

  require Logger

  def get_all() do
    data = SimulationService.Repository.list()
      |> Enum.map(&JSON.normaliseMongoId/1)
      |> Enum.to_list()
      |> Jason.encode!()
    {:ok, data}
  end

  def get_by_id(id) when is_binary(id) do
    try do
      data = BSON.ObjectId.decode!(id)
      |> SimulationService.Repository.get_by_id()
      |> JSON.normaliseMongoId()
      |> Jason.encode!()

      {:ok, data}
    rescue
      _ in FunctionClauseError -> {:not_found, "Simulation does not exist"}
      _ -> {:server_error, "An error occurred internally"}
    end
  end
  def get_by_id(_), do: {:not_found, "Simulation does not exist"}

  def get_by_ref_id(ref_id) do
    try do
      case SimulationService.Repository.get_by_ref_id(ref_id) do
        nil ->
          {:not_found, "Simulation does not exist"}
        raw ->
          data = JSON.normaliseMongoId(raw)
          |> Jason.encode!()
          {:ok, data}
      end
    rescue
      _ in FunctionClauseError -> {:not_found, "Simulation does not exist"}
      _ -> {:server_error, "An error occurred internally"}
    end
  end

  def create(data) do
    try do
      case SimulationService.Schema.changeset(data) do
        %{changes: changes, errors: _, valid?: true} ->

          case SimulationService.Repository.get_by_ref_id(changes.tierId) do
            nil ->
              case SimulationService.TierRepository.get_one(changes.tierId) do
                %{body: _,  status_code: 404} -> {:malformed_data, "Tier must be exist"}
                %{body: tier,  status_code: 200} ->
                  tier
                  |> Map.take(["annualRevenue", "enterpriseNumber", "legalName", "naturalPerson", "nacebelCodes"])
                  |> Map.put("deductibleFormula", changes.deductibleFormula)
                  |> Map.put("coverageCeilingFormula", changes.coverageCeilingFormula)
                  |> SimulationService.QuoteRepository.make_one()
                  |> case do
                    %{body: quote_entity,  status_code: 200}  ->
                      {:ok, mongo_result} = quote_entity
                      |> Map.put("tierId", changes.tierId)
                      |> SimulationService.Repository.create()

                      data = SimulationService.Repository.get_by_id(mongo_result.inserted_id)
                      |>JSON.normaliseMongoId()
                      |> Jason.encode!()
                      {:ok, data}
                    %{body: errorBody,  status_code: _}  ->
                      {:malformed_data, errorBody["error"] || "Can be create Simulation"}
                  end
              end

            _ -> {:malformed_data, "Duplicate found"}
          end

        %{changes: _, errors: _, valid?: false} -> {:malformed_data, "Unprocessable Entity"}
      end
    rescue
      ici ->
        Logger.info(ici)
        {:server_error, "An error occurred internally"}
    end
  end
end
