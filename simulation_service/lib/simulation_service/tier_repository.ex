defmodule SimulationService.TierRepository do
  require HTTPoison
  require Logger

  def get_one(id) do
    headers = ["Content-Type": "application/json"]
    {:ok, response} = HTTPoison.get("#{Application.get_env(:simulation_service, :tier_service_url)}/by_id/#{id}", headers)
    {:ok, bodyMap} = Jason.decode(response.body)
    %{status_code: response.status_code, body: bodyMap}
  end
end
