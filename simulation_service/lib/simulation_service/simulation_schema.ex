defmodule SimulationService.Schema do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key  {:id, :binary_id, autogenerate: true}

  schema "simulation" do
    field :tierId, :string
    field :deductibleFormula, :string
    field :coverageCeilingFormula, :string
  end

  def changeset(params) do
    %__MODULE__{}
    |> cast(params, [:tierId, :deductibleFormula, :coverageCeilingFormula])
    |> Map.take([:changes, :errors, :valid?])
  end
end
