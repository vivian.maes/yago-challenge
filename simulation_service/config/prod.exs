import Config

config :simulation_service, port: 80

config :simulation_service, database: "simulations_db"
config :simulation_service, database_host: "db"
config :simulation_service, pool_size: 3
config :simulation_service, tier_service_url: "http://tier_service"
config :simulation_service, quote_service_url: "http://quote_service"
