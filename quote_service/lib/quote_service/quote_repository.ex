defmodule QuoteService.Repository do
  require HTTPoison

  def create_quote (info) do
    body = info |> Jason.encode!()
    headers = ["X-Api-Key": Application.get_env(:quote_service, :api_token), "Content-Type": "application/json"]
    {:ok, response} = HTTPoison.post(Application.get_env(:quote_service, :api_url), body, headers)
    response.body |> Jason.decode!() |> Map.fetch("data")
  end
end
