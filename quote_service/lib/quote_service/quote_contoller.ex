#TODO: Attention :
#   Pas de gestion des erreur technique
#   Erreur fct uniquement relay
#   ... à faire si le temps le permet
defmodule QuoteService.Controller do
  def get_quote(info) do
    {:ok, response } = QuoteService.Repository.create_quote(info)
    case response["available"] do
      true -> {:ok, response |> Jason.encode!()}
      false -> {:malformed_data, response["message"]}
    end
  end
end
