defmodule ExpertService.JsonTOOLS do
  def normaliseMongoId(doc) do
    doc
    |> Map.put('id', doc["_id"])
    |> Map.delete("_id")
  end
end
