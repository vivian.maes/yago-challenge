defmodule ExpertService.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application
  require Logger

  @impl true
  def start(_type, _args) do
    children = [
      {
        Plug.Cowboy,
        scheme: :http,
        plug: ExpertService.Router,
        options: [port: Application.get_env(:expert_service, :port)]
      }
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: ExpertService.Supervisor]
    # Logger.info "Le service de tiers ecoute sur le port: #{port()}..."
    Supervisor.start_link(children, opts)
  end
end
