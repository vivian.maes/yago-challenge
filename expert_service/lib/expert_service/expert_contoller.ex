defmodule ExpertService.Controller do
  def filter_list_by_strings(list) do
    Enum.filter(list, fn item  ->
      Regex.match?(~r/^862/,"#{item}")
    end)
  end
  require Logger

  def make_augure(data) do
    Logger.info(data)
    augure = filter_list_by_strings(data["nacebelCodes"])
    |> Enum.count()
    |> case do
      0 ->  %{"augure" => []}
      _ ->  %{"augure" => [
                "formule déductible \"PETIT\" car cela réduira votre coût et ce n'est pas si important pour vous (car la probabilité est limitée)",
                "Formule plafond de couverture \"LARGE\" car cela vous protégera pour des montants beaucoup plus élevés que celui par défaut en cas de conséquences dangereuses de votre action (mauvais médicament qui tue le patient par exemple).",
                "une couverture de frais juridiques est fortement recommandée dans votre cas car les risques sont élevés. Il y a une forte probabilité que la réclamation soit suivie d'actions en justice. Vous devez donc pouvoir vous défendre."
      ]}
    end
    |> Jason.encode!()
    {:ok, augure}
  end
end
