defmodule ExpertService do
  @moduledoc """
  Documentation for `ExpertService`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ExpertService.hello()
      :world

  """
  def hello do
    :world
  end
end
